# Mathématiques de la sécurité

```
38 89
k
38k = 1 (mod 89)
```

`ma + nb = 1`

Tout d'abord diviser 89 par 38 et récupérer le reste :
```
89 = 2 * 38 + 13
38 = 2 * 13 + 12
13 = 1 * 12 + 1
```

```
1 = 13 - 1 * 12
    = 13 - 1 * (38 - 2 * 13)
    = 13 - 1 * 38 + 2 * 13
    = -1 * 38 + 3 * 13
    = -1 * 38 + 3 * (89 - 2 * 38)
    = -7 * 38 + 3 * 89

-7 * 38 = 1 (mod 89)
k = -7
k = -7 + 89 = 82
```

89 premier ?

Z/89Z corps

(Z/89Z)+ 88 éléments inversibles

``35^88 = 1 (mod 89)``

par le petit théorème de Fermat

m E Z/89Z
```
c = 35
m -> mc = k (mod 89)
k -> k^d = m
m = k^d = m^(cd) = m^1
cd = 1 mod (phi(n)) = mod (phi(p)) = mod (p-1)
cd = 1 mod (88)

2^11 = 1 (mod 890)
? 2 générateurs de (Z/89Z)* ?

1, 2, 2^2 = 4, 2^3,... 2^10 || 11 éléments
2^11 = 1, 2^12 = 2
```

L'ensemble des 2^n a 11 éléments
(Z/89Z)* a 88 éléments

mod(phi(p)) = mod(p-1)

```
mod 19
1
3
3^2 = 9
3^3 = 27 = 8
3^4 = 24 = 5
3^5 = 15
3^6 = 45 = 7
```
