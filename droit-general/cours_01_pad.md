# Droit général - Cours 1

## Sources du droit:
1ere ?
2e source du droit : Décision de justice : n'a pas de valeur mais essentiel sur le point de vue pratique
3e source : Coutumes doctrines

## Les textes sont hiérarchisés entre eux.

Au sommet : la constitution (4 oct 1958, 5ème république)
La constitution = repartition du pouvoir, le fonctionnement de l'Etat.

Valeurs du peuple français, droits fondamentaux :

* droit à la vie (lol)
* respect de l'intégrité physique
* liberté d'expression
* droit d'Habeas Corpus (ne pas être emprisonné sans jugement)
* inceste : Point important

Les enfants battus pensent que c'est normal d'être battus et culpabilisent.
Les relations sexuelles parent-enfant sont interdites par la loi.

Un texte ne peut être modifié que par ceux qui l'ont fait (l'organisme).

La constitution peut être modifiée de deux manières :

* Soit par référendum
* Soit par le congrès (A.N et Sénat réunis, majorité aux 3/5).
"Le public est imbécile et s'intéresse aux histoires de fesses du président de la république (Point important)
Tous les débats sont retranscris sur des registres consultables (se renseigner sur les sites de l'assemblée nationale ou du sénat)

Tous les textes doivent être conformes et il faut laisser des moyens de vérifier. 2 moyens :

* conseil constitutionnel : rôles importants. Ils peuvent selon leur lecture de la constitution avoir une influence énorme sur les lois qui vont être votées en France.
    * 9 personnes
    * 1/3 renouvelé tous les 3 ans
    * 1 par le président, 1 par l'assemblée generale, 1 par le président du Sénat
    * pas de cumul des mandats
    * on rajoute d'anciens présidents de la République si l'envie leur prend.
    * Plusieurs roles:
        * abroger les lois qui ne lui plaisent pas
        * (depuis 2010) Question Prioritaire de Constitutionnalité, le citoyen lambda peut dire stop au juge ( non respect de la constitution ), tranfert au conseil constitutionnel

Sujet de partiel: Autorisation des corridas et des combats de coqs à certains endroits du territoire français
=> pas le droit de torturer des animaux/pokemon/chatons (chatons  = animaux)

Les FAI c'est des enculés. C'est anticonstitutionnel de surveiller leur traffic.

On est tellement à la pointe que le droit n'est pas à la pointe.
Le comportement précède toujours le droit.

Quand on s'appelle Google on lance une activité et on règle les soucis juridiques après, quand on est citoyen on a pas les moyens


Sous la constitution:

* Les traités internationaux : ne peut être que conforme à la constitution. Adoption (majorité des 3/5 ou referendum)
  (On ne peut utiliser un referendum que pour l'adoption d'un traité ou pour une modification de la constitution)
  Il y a deux types :
* des traités qui organisent les relations des états entre eux (frontières...)
* des traités qui traitent de matière (exemple : droit du travail) beaucoup plus thématiques

Des grands principes :
* Un minimum de règles communes que toute personne qui adhère à ce traité s'engage à respecter
* Le droit international ne regle pas les problèmes, il dit quelle loi de quel Etat on doit appliquer. Ce n'est pas un droit SUPRAnational. Règle les conflits de compétences

