<!--
%cours: Droit Général
%authors: Dsensei
%date: 2014-11-14
-->

En pénal l'infraction doit être intentionnelle. (intentionnelle=>conscience)

Personne mise en examen <=> Personne présumée innocente

Non lieu != relaxe : en effet non lieu à cause d'un vice de procédure.

Le code civil est né de la volonté de Napoléon de créer un texte unique et
uniforme valable sur tout le territoire français. (Ce qui n'était pas le cas
avant). Il a prit 4 juristes de l'époque (de 4 régions diff) en leur donnant
pour mission de créer ce code civil. Ce code devait être à la fois la synthèse
de chaque droit de chaque région, et à la fois devait fixer des règles de droit
convenant à chaque français.

Le code civil reste donc le droit commun.

Le code civil joue un rôle primordial.

Lorsque Google invente des services par exemple, l'article 1382 sera utilisé si
aucune loi n'existe sur ces nouveaux services.

Le droit du travail a ses propres jurisdictions (droit relation salariés
(prudhommes) - prestations sociales (tribunaux social))

Le droit commercial (régit les relations avec les commerces) a ses propres
jurisdictions.

On a enormement de codes/droits pour chaque matière, autres exemples:

* Code de l'éducation Nationale
* Propriété intellectuelle
* Code de la route
* ...


## L'organisation juridictionnelle

En France l'ensemble des juridictions est divisée en deux ordres:

* Administratif
  * Droit public

* Judiciaire
  * Droit privé
  * Droit pénal

C'est le principe de séparation des pouvoirs.

La mise en application de cette séparation en France a donné cette
organisation:

<img src="http://www.assurdem.fr/images/ordre-judiciaire.jpg"></img>

<img src="http://jurigeek.org/wp-content/uploads/2012/03/Organigramme-de-lorganisation-judiciaire.png"></img>

Parfois on ne sait pas si l'on a affaire à la juridiction publique ou privée,
dans ce cas il y a le tribunal des conflits.

Chacun des deux ordres est un ensemble hierarchisé de juridictions articulées
entre elles par le biais des voies de recours, et chaque ordre a une
juridiction supérieure (administratif -> Conseil d'état / judiciaire -> cour de
cassation).

Conseil des prud'hommes: règles les litiges entre les empoyeurs et les salariés
(secteur privé). Deux particularités: divisé en 5 ections (commerce, industrie,
encadrement, agriculture, ativités diverses). Chaque section est divisée en 2:
un bureau de conciliation (un employeur, un salarié) et presque toutes les
affaires commence pour la majorité par la recherche d'une conciliation.

En droit privé on peut mettre fin à un jugement n'importe quand si on trouve un
compromis.

Si on a pas trouvé de compromis on passe devant le bureau de jugement (deux
employeurs - 2 salariés)

Note: dans un tribunal on peut avoir plusieurs chambres.

Devant le tribunal de gde instance, la représentation par un avocat est
obligatoire. il y a des sous instances avec des juges uniques (execution,
affaires familliales, enfants, tutelles...)
On est de plus en plus dans un regroupement des instances.

Les tribunaux d'instance : un seul juge. La répartition du contentieux se fait
de deux manières:
* Par matière/Compétences : il y a des matières réservées à tel ou tel tribunal
* Par montant des demandes : 
  * +10K€      -> Tribunal de grande instance
  * -10k€/+4k€ -> tribunal d'instance
  * -4K€       -> Tribunal de proximité




Juridictions pénales (état qui poursuit le délinquant):

S'il y a une victime, c'est un témoin priviligié, on lui propose de se porter
partie civile et donc d'avoir un avocat, elle a alors un mot à dire.

Dans un tribunal il y a une répartition des tâches: Ce sont les juges qui
mènent le débat.

En cas de crime organisé, c'est trop dangereux donc au lieu d'avoir un juri
populaire donc on a un juri constitué de magistrats

* Cours d'assises : crimes
  * condanation demandée par le parquet et prononcée par le juri
  * Juridiction particulière traduisant la justice populaire
  * Juri populaire (composé de citoyens classiques)

* Tribunal correctionnels : délits (punis de peines > 2000€ || 10ans de prison)
  * Collégial: 3 magistrats professionnels (souvent avec un seul juge)

* Tribunal de Police : contraventions de 5e classe
  * Magistrat professionnel

* Juridiction de proximité est compétante pour les infractions de petite <= 4
classes

Pour certains types d'infractions (condamnations automatiques), l'état peut
habiliter un certain nombre de personnes

Il y a aussi beaucoup de petites autorités (CNIL, AMF, ...) qui sont des
administratives indépendantes, et ont un pouvoir autonome de sanction
(pécuniaires).

Tribunal : Lorsque l'on saii le juge, on commence par le premier degré.

En France il y a possibilité de faire un recours au *premier* degré, toute
décision de justice est contestable. Chaque tribunal fixe un montant, et tri les
petites et grosses affaires (de recours).

Petites affaires -> cour de cassation

L'appel c'est un *second* recours. Les juges reprennent alors les faits dans
leur intégralité. Appelés *Les juges du fond*. Soit elle confirme la décision
prise en premier degré, soit c'est sa décision qui s'applique.

Cependant le premier jugement peut demander à ce que l'application du jugement
soit réalisée pendant le deuxième jugement.

Si l'accusé ne fait pas appel, l'application du jugement se fait immédiatement
après la période offerte pour faire appel.

Différence entre les fait et le droit: Un atteinte à la vie privée est tolérée
au nom de l'humour si elle ne porte pas atteinte à la dignité humaine.

Les faits cela relève de l'apréciation souveraine des juges du fond.

