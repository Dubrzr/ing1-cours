<!--
%cours: Droit Général
%authors: Dsensei
%date: 2014-10-31
--> 

Classement entre les différentes branches du droit :

* Le droit publique
* Le droit pénal
* Le droit privé (ou civil)

## Droit Publique : 

Dépend de jurisdictions (conseil administratif, conseil d'état)

* Ensemble des règles (droit de l'état)
* Régir la rédaction de rapports entre l'etat <-> les citoyen <->

Exemples:

* Tout ce qui concerne la réglementation de la sécurité au travail
* Conflit de droit public : quelqu'un qui contracte une maladie dans un hopital
publique -> conflit entre une personne et un organisme publique.

## Droit pénal :

### C'est quoi ?

C'est l'état qui institue et amménage le droit de punir (les gens qui ne
respectent pas la loi).

###Ce qui le distingue :

Logique représsive -> la sanction est en fonction de la gravité de l'infraction
L'intensité de la sanction dépend de la gravité de l'infraction mais pas du
dommage cause à une eventuelle victime.

C'est beaucoup plus grave de tuer quelqu'un et de le louper que de le tuer sans
le vouloir.

La victime n'a cependant aucun pouvoir sur l'avis de l'état, c'est l'état qui a
la possibilité de poursuivre, contre l'agresseur, mais aussi sur l'avis de la
victime.

Signaler : informer l'état que la loi pénale n'a pas été respectée (au près de
la police, du procureur, d'organismes). D'un point de vue juridique on ne peut
pas poursuivre quelqu'un qui a signalé. (Cependant faire attention à la non
assistance à personne en danger).

On attaque pas pénalement : on porte plainte, on se porte partie civile.

Toutes les lois contiennent des dispositions pénales.

Une autre particularité : les dispositions pénales sont éparpillées dans
toutes les lois.
En droit pénal, tout ce qui n'est pas interdit par la loi est autorisé.

Element intentionnel : intention présumée (nul n'est censé ignorer la droit)
sauf dans le cas d'une démance avérée

En droit civil cependant on peut être sanctionné pour le "non port d'une tenue
correcte" -> le juge pourra faire l'interpretation qu'il en voudra
