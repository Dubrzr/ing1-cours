<!--
%cours: Droit Général
%authors: dsensei
%date: 2014-10-10
--> 


## Info Evaluations
* peut-être qcms
* questions sur deux textes de droit

<!--
## COURS 01: 10/10/2014
-->

# Quelles sont les soures du droit ?

'''
+ (Quelle source vais-je chercher si je me pose une question ?)
'''

On dispose de 3 grands types de sources :
* formelles/ethatiques = lois/reglements... : réalisés par les autorités.
* jurisprudence : les decisions de justice
* Coutumes/doctrines/recherches



## Sources ethatiques

Tous les textes sont hierarchisés entre eux, au  sommet de cette pyramide, on trouve en France, la **constitution**.
Quand on parle de constitution (ou bloc de constitution), on parle de la notre (4 Oct 1958): la 5e, et au même niveau on trouve la declaration des droits de l'homme.

Ces textes contiennent l'organisation de l'état (comment l'état est organisé ? Qui fait quoi ? Rôle des assemblées ? Etc <=> le fonctionnement de l'état).
En résumé, ces textes rapellent les principes/valeurs fondamentaux auxquels le peuple Français est attaché.

Parmi ces valeurs on a les droits fondamentaux (liberté d'opinion, droit à la vie, droit à la vie privée, droit d'expression, droit à l'intégrité physique etc). 
Ces droits sont au sommet de la hierarchie des droits.

Un texte ne peut être modifié que pas ceux qui l'ont rédigé. Et ce, par référendum, ou par le congrès (assemblée nat. + Sénat se réunissent ensemble) avec majorité des 3/5e.

Constitution > traités internationaux > loi > arretés/décrets

Tous les textes de loi/règles de droit ont des caractères communs : elles sont
générales, permanentes, obligatoires et assorties d'une sanction.

* Générale car elle s'applique à tout le monde.
* Permanente car elle s'applique jusqu'à suppression/remplacement (pas d'effet
rétro-actif)
* Obligatoire pour tout le monde y compris pour le juge
* Caractère de sanction : une règle n'ayant pas de sanction, n'est pas une
règle de droit. 

## Les sanctions

Il y en a 2 types :

+ Pénales (amendes/peines de prison) - selon la gravité de l'acte
+ Civiles : pas de punitions, logique de réparation : droits & intérêts.

La difficulté est de trouver une sanction juste, proportionnée et dissuasive.

Ex : Les nouvelles formes de délinquance

Pour vérifier la conformité, 2 moyens ont été mis en place :

+ Le conseil constitutionnel : ils peuvent avoir une influence enorme sur les
lois qui vont être votées en France (constitué de 9 personnes (+ les anciens
présidents de la république s'il le veulent), renouvelé tous
les 3 ans (par le Présenti, prés. assemblée, prés. sénat) - pas de cumul)
Vérifie que les lois sont conformes à la constitution. Le conseil peut être
saisi par la QPC (question prioritaire de constitutionnalité) : citoyen au cours d'un procès considère qu'une loi pose un
problème. Le conseil répondra une fois mais pas deux. (Toute loi pouvant être
contestée).

Dans des secteurs dits de pointe, le droit ne suit pas car ces secteurs vont
plus vite que le droit, dans ces cas là une QPC peut être posée.

+ Les traités internationnaux : ne peuvent être QUE conforme à la constitution.
S'il n'est pas conforme, on le rend conforme ou on ne l'accepte pas, ou on
modifie la constitution. Adoptable par référendum.

Deux types de traités:
* Ceux qui s'occupent de l'org des états entre eux (gérés par l'ONU)
+ Traités qui sont beaucoup plus thématiques : contiennent un minimum légal
(droit du travail...)

Intéret des traités : règle les problèmes de compétence et de droit à
appliquer. Quels droit appliquer ? Le droit internationnal ne règle pas les
problèmes, il dit quelle loi de quel état il faut appliquer. Ce n'est pas un
droit "supra-nationnal"

Exemples concrets :
-> Je vais travailer dans une entreprise FR en Angleterre.
--> Le droit de quel pays j'applique ??? Demande au droit international pour savoir.


## Le droit de l'UE : 
Il occupe une place à part car ce droit est *hybride* (intl & nationnal).
Il est composé de traités intl, qui ont la particularité de créer des
instituions qui auront des textes directement applicables 

4 types de textes dérivés : 
+ Directives
+ Règlements (directement applicables ds les états membres)
+ Recommendations (osef)
+ Décisions des cours européennes (CJUE - CEDH)

Ex: La monnaie unique a entrainé la création d'une institution (BCE) qui fixe
les taux directeurs.

Avoir les mêmes règles de droit ne permet pas de créer une communauté, mais ça
leur permet de se sentir dans une communauté.

## 3 familles de matières de lois :
+ Celles pour lesquelles seul le parlement peut intervenir (fondamentaux de la
république / des dh ...)
+ Celles dites "mixes" pour lesquelles on utilisera un processus bcp plus
rapide
+ Les décrets d'application : voies règlementaires



# Différentes étapes pour un projet de loi.

1e étape : porté devant les règlementaires (assemblée nat.) ->
texte étudié en commission (eventuellement modifié : amendemants) -> soumis au
début en séance publique (souvent fortement amendé) -> Vote par les députés

2e étape : Passage à la chambre du sénat -> même procédure : étude en
comission, discussions, vote.

Plusieurs cas : 
+ Assemblée et Sénat d'accord -> Pas de soucis la loi est mise en place (arrive
très rarement)
+ Assemblée et Sénat pas d'accord -> Retour devant l'assemblée (en boucle
jusqu'à ce qu'ils trouvent un accord, ou que le gouvernement intérrompe au bout
de deux lectures - ou *navettes* - puis une commission mixte paritaire sera
alors chargée d'établir un texte qui mette tout le monde d'accord, et s'ils n'y
parviennent pas, c'est l'assemblée nationnale qui aura le dernier mot - car ils
sont élus au suffrage universel direct).

3e étape : Promulgation par le chef de l'état (protocolaire).

4e étape : Publication de la loi au journal officiel.

Il existe deux manières d'accélerer l'application de la loi:
+ Le recours aux ordonnances
+ Article 49.3 : Le gouv. peut faire accepter un texte sans vote (l'assemblée
peut quand même sensurer le texte).



# Connaissance de la loi

Nul n'est censé ignorer la loi *dans son secteur d'activité*, les juges
appliqueront cela en grande majorité. Cela s'applique aussi quand l'on est à
l'étranger.

