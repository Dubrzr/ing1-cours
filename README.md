```

  ___ _   _  ____ _            ____                     
 |_ _| \ | |/ ___/ |          / ___|___  _   _ _ __ ___ 
  | ||  \| | |  _| |  _____  | |   / _ \| | | | '__/ __|
  | || |\  | |_| | | |_____| | |__| (_) | |_| | |  \__ \
 |___|_| \_|\____|_|          \____\___/ \__,_|_|  |___/
                                                        

```

# Repo pour les Cours d'ING1

## informations

Ce repo est destiné à la création de pdfs et de pages web.
Vous pourrez bientôt générer les pdfs pour tous les fichiers avec make pdfs.
Concernant les pages web, un site web sera mis en ligne et actualisé fréquemment.

## Comment participer ?

Ce repo est ouvert, c'est  pourquoi vous pouvez [pull request](https://help.github.com/articles/using-pull-requests/) ou demander l'accès au root (si vous le connaissez...).

## Writing style

Afin d'obtenir des cours propres, merci de respecter les règles suivantes:

+ La rédaction des cours se fait en [Markdown](https://help.github.com/articles/markdown-basics/)
+ Le nom des fichiers doit suivre la forme suivante : xx_nom_du_fichier.md (xx: 01->99)
+ Vous devez commencer chaque fichier par le header suivant :
```
<!--
%cours: <nom du cours>
%authors: <login1>, <login2>...
%date: yyyy-mm-dd
--> 
```

# Liste des cours disponibles

## Bases du Marketing
## Computer Architecture and Assembly Language
## Droit général
## Gestion Projet
## Programmation Linéaire
## Qualité
