## Marketing Mix (4P)

4P =  Produit, Prix, Place (distribution) et Promotion (communication)

*Marketing Mix* : ensemble des outils dont l’entreprise dispose pour atteindre ses objectifs auprès du marché-cible. (Kotler & Dubois)

Une entreprise peut chosir d'investir plus dans un P plutôt que dans un autre (exemple: + de promotion pour un prix plus élévé = apple)

4P = 

```
Produit
Prix
Placement
Promotion
```

## Le "4P + 4P"

```
Produit
Prix
Placement
Promotion
+
Personnel
Partenaires
Process
Package
```


## Les "4C"

```
4 P		  ->	4 C
Produit	  ->	Client
Prix	  ->	Coût
Place	  ->	Commodité
Promotion ->	Communication
```

## Matrice d'Ansoff

*Définition:* C'est un outil de planification de croissance

<img src="https://linx.li/selif/222qvw52g.jpg"></img>

## Matrice BCG  (*Boston Consulting Group*, fin des années 1960) 

*Définition:* C'est un outil qui permet de justifier des choix d'allocation de ressources

<img src="https://linx.li/selif/sh109cksd.jpg"></img>

## SWOT, outil d'audit marketing

*Définition:* Outil permettant de déterminer les options stratégiques d'une entreprise selon les menaces, les faiblesse et opportunités.


```
		  |-----------------------|
		  |  Positif  |  Négatif  |
|---------------------------------|
|         |			  |			  |
| Origine |  Forces	  |Faiblesses |
| interne |     S	  |    W	  |
|		  |(strengths)|(Weakness) |
|---------------------------------|
| 		  |						  |
| Origine |Opportunité|  Menaces  |
| externe |     O	  |     T     |
|   	  | opport.   |  Threats  |
|---------------------------------|
```


## Les 5 forces de Porter

<img src="https://linx.li/selif/u3aqy5y1v.gif"></img>

L'intensité de la concurence se fait par:

* Menace des nouveaux entrants sur le marché
* Pouvoir de négociation des clients
* Pourvoir de négociation des fournisseurs
* Produits de substitution
```
On peut y ajouter les législations/Réglementations/Restrictions contraignantes,
qui favorisent les entreprises qui les appliquent : cela leur permet de se
différencier de la concurence (ex : reglementations concernant l'écologie)
```


## Les 4E / marketing emotionnel

Il consiste à remplacer les 4P par les 4E :

```
4 P		  ->	4 E
Produit	  ->	Emotion
Prix	  ->	Exlusivité
Place	  ->	Expérience
Promotion ->	Engagement
```

## Les metiers du Marketing



# 2 - Marché, consommateurs, et processus d'achat

## Marché-

### Les types de marchés

* Marché générique
-> Produits différents pour un même marché
-> Ex : Tourisme / Boissons rafraichissantes sans alcool

* Principal
-> Produits senblables et concurents
-> Ex : Sodas / Voyages organisés

* Segments
-> Découpage du marché principal en groupe homogènes de produits 
-> Ex : Colas, Boissons gazeuses aux arômes de fruits / Croisières


* Marché substitut
-> Produits de nature différente à ceux du produit ppal mais qui procurent des
satisfactions équivalentes ou voisines.
-> Ex : Potentiellement toutes les autres boissons (jus de fruits, sirops,
eaux, ...) / Nautisme

* Marché complémentaire
-> Produit qu'il est nécéssaire de consommer avec le produit principal
-> Ex : Rasoirs (lames de rasoir) / Imprimantes (cartouches) / Véhicules
(assurances=


* Marché support
-> Ensembled de produits auxquels recourt le marché principal
-> Ex : Fontaines à eau / Transport aérien, maritime, routier, hôtellerie


### Le marché pertinent

#### Définition :

"Le marché pertient d'une entreprise pet être défini comme l'ensemble des
produits, services ou comportements avec lesquels elle est en concurrence et
contre lesquels elle peut espérer, par son action propre, lutter avec
une certaine efficacité." ***(Lindon et Jallat, 2002)***

#### Difficulté :

Pour la plupart des produits il est facile de déterminer le marché pertinent
(ex: le vin, les pâtes, les autobus, ...)

Mais certains produits peuvent être rattachés à plusieurs catégories
différentes.

Ex : le yaourt à boire peut être considéré comme un "produit laitier" ou comme
une "boisson rafraichissante".

### Description du marché

```
Comment estimer un marché ?
```

* Nombre d'acheteurs
* Valeur / Chiffre d'affaires
* Volume (de produit)

### Taille du marché

#### Marché réel :

Volume des ventes effectives du produit et de l'ensemble des produits qui lui
sont substituables.


#### Marché actuel :

Marché actuel de l'entreprise + marché actuel de la concurence 

#### Marché potentiel :

Niveau maximum que pourraient atteindre les ventes du produit
=> Population totale - non-consommateurs absolus + non-conommateurs relatifs.

##### Définitions :

* non-cons. abs. = Ne peuvent pas cons. pour des raisons phys/relig/morales
* non-cons. rel. = Pas encore clients, ou non-clients volontaires/involontaires :
non-connaissance (Potentiel de buisiness)


#### Marché des prospects : (Suspecter des futurs clients)

* non-cons. rel.
* clients des concurents

```
Suspects --()--> prospects -> Leads (qui ont des pistes)
--(project/budget/décideur)--> Opportunité -> client
```

### Les acteurs du marché :

* Consommateurs
-> Consommer les produits proposés sur le marché
-> Qui sont-ils ? / Combien ? / Comportement ? / Motivations ? /
Caractéristiques ?

* producteurs
-> Alimenter le marché en fonction des besoins
-> Politique du produit ? / Politique du prix ? / Politique de distribution ? /
Politique de communication ?

* Prescripteurs et Conseillers
-> Informer et conseiller les acteurs et les consommateurs
-> Qui sont-ils ? / Combien sont-ils ? Caractéristiques générales ? /
Comportement, habitude d'information ?

* Distributeurs
-> Faire parvenir les biens ou les services aux consommateurs
-> Lieu d'implantation ? / La gamme ? / La taille ? / Stockage ? /
Qualifications ? / SAV ?


### La segmentation

#### Les notions de la segmentation

Un segment = nb prospects qui ont des caractéristiques communes qui vont nous 
permettre de les adresser de manière globale.

La **segemntation d'un marché** peut se définir comme l'identification de 
sous-ensembles **d'acheteurs artageant des besoins et des comportements d'achat 
similaires**.

La terre se compose de millieards de consommateurs potentiels ayant leur 
propres ensembles de beoins, de comportements et de pouvoir d'achat

La segmentation vise à réaliser des groupes d'acheteurs, aux besoins et comportements
similaires. **Un tel groupe est nommé segment**.

#### 3 Ppaux types de variable de segmentation :

* Besoins (pourquoi acheterai-t-il ce produit ?)
  * Fréquence de consommation
  * Lieux d'achat

* Comportements
  * Fréquence de consommation
  * Quantités consommées
  * Lieux d'achat
  * Fidélité à la marque
* Caractéristiques des individus
  * Démographiques (âge, sexe...)
  * Socio-culturelles (classe sociale,,niveau d'études...)
  * Psychographiques (modes de vie, Religion, croyances diverses...)

##### Processus de segmentation :

```
Choix d'un marché \ Population concentrée par un type de besoin /
                   \-------------------------------------------/
Classification de   \       variables de segmentation         /
la population        \                                       /
                      \-------------------------------------/
Choix des segments     \         Segments centrés          /
                        \---------------------------------/
                         \                               /
                          \            CIBLES           /
                           \---------------------------/
```

## Les consommateurs

### Pyramide de Maslow

*Basée sur l'hypothèse d'une hiérarchie de besoins*

#### Les besoins

  * Besoin physiologiques (fain, soif, santé, reproduction)
  * Besoin de sécurité (Abri, protection physique et/ou morale, besoin relatif à la sécurité, à la santé)
  * Besoin d'appartenance (Famille, amis, communauté, groupe social...)
  * Besoin d'estime (Prestige, réussite..., besoin de reconnaissance, d'être respecté, d'avoir un rang social...)
  * Besoin d'accomplissement (Maîtriser, comprendre, s'exprimer, se dépasser..., besoin de dépassement de soi, de valeur personnelle, de sentir la vie, de repousser ses limits.

### Classification de Muray

*Fin des années trente, le psychologue André Muray a proposé cette liste de
besoins.*

#### Les besoins

  * Besoin d'acquérir (Posséder, avoir de la propriété)
  * Besoin d'accomplissement (Exercer, une responsabilité, surmonter les obstacles)
  * Besoin d'exhibition (Attirer l'attention, exciter, choquer...)
  * Besoin de dominance (Influencer ou contrôler d'autre autrui, guider et diriger, organiser la vie d'un autre)
  * Besoin de jeu (Se divertir)
  * Besoin d'ordre (Arranger, organiser, être précis...)
  * Besoin de reconnaissance (Rechercher la distinction, le prestige social et les hommes)
  * Besoin de référence (Admirer et suivre de son plein gré un supérieur, coopérer et servir)
  * Besoin d'autonomie (Rechercher la liberté et lutter pour son indépendance)
  * Besoin d'agression (Attaquer, accuser, blâmer, punir, tuer)

### Les forces psychologiques

#### Motiviations

  * Hédoniste
  * Oblatif
  * Auto-expression

#### Freins

  * Inhibition
  * Peurs

### Les attitudes

#### Chemin comportemental

  * Besoins
  * Croyances
  * Evaluation
  * Décision

#### 3 Elements clefs

  * Le **congnitif** (Croyances à l'égard de l'objet, ce que l'on sait)
  * L'**affectif** (sentiments envers l'objet, ce que l'on en pense)
  * Le **conatif** (intentions vis-à-vis de l'objet, ce que nous sommes prêtes à faire pour l'acquérir)

### Modèles

#### Processus réfléchi

Congnitif -> Affectif -> Conatif. (modèle de l'apprentissage conatif -
processus suivi lors des achats raisonnés - produits chers, consommateur très
attentif...)

#### Implicatioin minimale

#### Dissonance cognitive

Conatif -> Affectif -> Cognitif (achat impulsif)

* Processus d'achat
  * La décision d'achat
    * Variables liées à l'individu
      * Expérience
      * Personnalité
      * Image de soi
      * Style de vie
    * Variables sociologiques
      * Le groupe
      * La famille
      * La culture
      * Les classes sociales
  * Les participants à la décision
  * Les étapes du processus
  * Elements impactant le processus d'achat

* Les participants à la décision
  * L'inspirateur
  * Le prescripteur
  * Le dénigreur
  * Le conseiller
  * Le décideur
  * L'acheteur
  * L'utilisateur ou consommateur
  * Le consom'acteur (connait un fort développement avec le web)
  * Le cadre législatif et réglementaire.

### Les étapes du processus

  * Prise de connaissance
  * Recherche d'informations
  * Définition de critères de choix (ou cahier des charges)
  * Evaluation des différentes solutions qui lui sont accéssibles
  * Prise de décision
  * Acquisition
  * Déploiement, consommation ou utulisation
  * Evaluation post-achat
  * Extension, Renouvellement

### Elements impactant le processus d'achat

  * Type de bien acheté
  * Degré d'implication du client
    * Neutre
    * Fort
  * Nature de l'achat
    * Routinier
    * Fréquent
    * Impulsion
    * Inhabituel
  * Besoins d'information et facilité ou difficulté d'accès à ces informations
    * Faible
    * Normal
    * Fort
  * Degré de différenciation de marque
    * Faible
    * Neutre
    * Fort
  * Nombre d'intervenants dans la décision
  * Mode de vente
    * Directe
    * Indirecte
  * Moyens d'accès au bien
  * Besoin de financement


## 3 Produit, Positionnement, Marketing Mix


## 4 Ecoute du marché, source de l'évolution de l'entreprise et de son offre
