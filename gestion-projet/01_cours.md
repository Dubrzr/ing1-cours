<!--
%cours: Droit Général
%authors: Dsensei
%date: 2014-10-10
--> 

## Evaluations
* peut-être qcms
* questions sur deux textes de droit

Sujet partiel : QPC  disposition autorisant corrida et combat de coq dans
certains endroit de France. La loi interdissant les sévices aux animaux, ->
principe de l'égalité pour tous devant la loi.

<!--
## COURS 01: 10/10/2014
-->

# Quelles sont les soures du droit ?

'''
+ (Quelle source vais-je chercher si je me pose une question ?)
'''

On dispose de 3 grands types de sources :
* formelles/ethatiques = lois/reglements... : réalisés par les autorités.
* jurisprudence : les decisions de justice
* Coutumes/doctrines/recherches



## Sources ethtiques

Tous les textes sont hierarchisés entre eux, au  sommet de cette pyramide, on trouve en France, la *constitution*.
Quand on parle de constitution (ou bloc de constitution), on parle de la notre (4 Oct 1958): la 5e, et au même niveau on trouve la declaration des droits de l'homme.
Ces textes contiennent l'organisation de l'état (comment l'état est organisé ? Qui fait quoi ? Rôle des assemblées ? Etc <=> le fonctionnement de l'état).
Ces textes rapellent les principes fondamentaux auxquelles le peuple Français est attaché.
Parmi ces valeures on a les droits fondamentaux (liberté d'opinion, droit à la vie, droit à la vie privée, droit d'expression, droit à l'intégrité physique etc). Ces droits sont au sommet de la hierarchie des droits.

Un texte ne peut être mdoifié que pas ceux qui l'ont rédigé. Et ce, par référendum, ou par le congrès (assemblée nat. + Sénat se réunissent ensemble) avec majorité des 3/5e.

Constitution > traités internationaux > loi > arretés...

Pour vérifier la conformité, 2 moyens ont été mis en plaec :

+ Le conseil constitutionnel : ils peuvent avoir une influence enorme sur les
lois qui vont être votées en France (constitué de 9 personnes (+ les anciens
présidents de la république s'il le veulent), renouvelé tous
les 3 ans (par le Présenti, prés. assemblée, prés. sénat) - pas de cumul)
Vérifie que les lois sont conformes à la constitution. Le conseil peut être
saisi par la QPC (question prioritaire de constitutionnalité) : citoyen au cours d'un procès considère qu'une loi pose un
problème. Le conseil répondra une fois mais pas deux. (Toute loi pouvant être
contestée).

Dans des secteurs dits de pointe, le droit ne suit pas car ces secteurs vont
plus vite que le droit, dans ces cas là une QPC peut être posée.

+ Les traités internationnaux : ne peuvent être QUE conforme à la constitution.
S'il n'est pas conforme, on le rend conforme ou on ne l'accepte pas, ou on
modifie la constitution. Adoptable par référendum.

Deux types de traités:
* Ceux qui s'occupent de l'org des états entre eux (gérés par l'ONU)
+ Traités qui sont beaucoup plus thématiques : contiennent un minimum légal
(droit du travail...)

Intéret des traités : règle les problèmes de compétence et de droit à
appliquer. Quels droit appliquer ? Le droit internationnal ne règle pas lles
problèmes, il dit quelle loit de quel étaat il faut appliquer. Ce n'est pas un
droit "supra-nationnal"

Exemples concrets :
-> Je vais travailer dans une entreprise FR en Angleterre.
--> Le droit de quel pays j'applique ??? Demande au droit international pour savoir.
